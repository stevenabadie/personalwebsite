from django import template
from taggit.models import Tag

register = template.Library()

@register.simple_tag
def get_taglist():
    queryset = Tag.objects.all()
    return queryset

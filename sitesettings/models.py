from django.db import models
from datetime import date

from wagtail.contrib.settings.models import BaseSetting, register_setting

@register_setting
class SiteSettings(BaseSetting):
    site_title = models.CharField(
        max_length=80,
        help_text='The title of your website'
    )
    site_tagline = models.CharField(
        max_length=160,
        help_text='The tagline of your website'
    )
    site_copyright = models.CharField(
        max_length=160,
        default=str(date.today().year),
        help_text='The copyright message for your website'
    )
    site_twitter = models.CharField(
        max_length=200,
        blank=True,
        help_text='The url for you twitter account'
    )
    site_mastodon = models.CharField(
        max_length=200,
        blank=True,
        help_text='The url for you twitter account'
    )
    site_gitea = models.CharField(
        max_length=200,
        blank=True,
        help_text='The url for you gitea account'
    )
    site_gitlab = models.CharField(
        max_length=200,
        blank=True,
        help_text='The url for you gitlab account'
    )
    site_github = models.CharField(
        max_length=200,
        blank=True,
        help_text='The url for you github account'
    )
    site_linkedin = models.CharField(
        max_length=200,
        blank=True,
        help_text='The url for you linkedin account'
    )
    site_googleauth = models.CharField(
        max_length=500,
        blank=True,
        help_text='Enter your google authenticator code'
    )

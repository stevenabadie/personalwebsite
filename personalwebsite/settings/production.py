from __future__ import absolute_import, unicode_literals

from .base import *

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = get_secret('SECRET_KEY')


# Security settings

SECURE_BROWSER_XSS_FILTER = True

SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True

X_FRAME_OPTIONS = 'DENY'

SECURE_HSTS_SECONDS = 31536000

SECURE_HSTS_PRELOAD = True


ALLOWED_HOSTS = [
    get_secret('HOST1'),
    get_secret('HOST2'),
    get_secret('HOST3'),
]

try:
    from .local import *
except ImportError:
    pass

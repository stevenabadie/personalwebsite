This is the source for the personal website of Steven Abadie.

It uses a number of Free and Open Source software packages.
* [Wagtail CMS](https://wagtail.io/)
* [Django](https://www.djangoproject.com/)
* [wagtailmenus](https://github.com/rkhleics/wagtailmenus)
* [Lightbox 2](http://lokeshdhakar.com/projects/lightbox2/)

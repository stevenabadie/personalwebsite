from django import template
from blog.models import BlogPage

from wagtail.core.models import Page, Orderable

register = template.Library()

@register.simple_tag
def get_last_post():
    queryset = BlogPage.objects.live().latest('date')
    return queryset

@register.simple_tag
def get_latest_posts():
    queryset = BlogPage.objects.live().order_by('-date')[1:6]
    return queryset
